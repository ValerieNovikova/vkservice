package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;


public interface UsersRepository extends CrudRepository<Long, User> {
}

