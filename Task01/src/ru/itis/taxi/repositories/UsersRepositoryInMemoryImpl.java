package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UsersRepositoryInMemoryImpl implements UsersRepository {

    private final Map<Long, User> users;
    private long counter;

    public UsersRepositoryInMemoryImpl() {
        this.users = new HashMap<>();
        this.counter = 0;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public void save(User entity) {
        users.put(counter, entity);
        entity.setId(counter);
        counter++;
    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(User entity) {

    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public User findById(Long aLong) {
        return null;
    }
}

